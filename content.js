'use strict';

console.log('UA-adjTSS: content script loaded');

const XP_workout_window = '//div[starts-with(@class, "workout ")]';
const XP_adjTSS_comment = '//div[@class = "commentBody" and starts-with(normalize-space(text()), "UA-adjTSS: ")]';
const XP_hrTSS = '//input[@id = "tssCompletedField"]';


function getElementsByXPath(xpath) {
  let res = [];

  let elems = document.evaluate(xpath, document.body, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
  for (let i = 0; i < elems.snapshotLength; i++) {
    res.push(elems.snapshotItem(i));
  }

  return res;
}


function focus_click_elem(xpath) {
  let elem = getElementsByXPath(xpath)[0];
  elem.focus();
  elem.click();
}


function get_workout_sport() {
  let res = null;
  let elem = getElementsByXPath(XP_workout_window);

  if (elem.length === 1) {
      res = elem[0].getAttribute('class').replace('workout ', '');
  }

  return res;
}


function get_adjTSS_comment() {
  let res = null;
  let elem = getElementsByXPath(XP_adjTSS_comment);

  if (elem.length === 1) {
      res = elem[0].textContent;
      console.log('UA-adjTSS: found comment "', res, '"');
  }

  return res;
}


function calculate_adjTSS(adjustment_type, hrTSS, duration, elev_gain, units) {
  // FIXME convert ft to m beforehand
  let res = hrTSS;

  if (adjustment_type === 'ascent') {
    if (units === 'metric') {
      res += Math.floor(elev_gain * 0.0328)
    } else if (units === 'imperial') {
      res += Math.floor(elev_gain * 0.01)
    }
  } else if (adjustment_type === 'time') {
    res = Math.floor(duration / 60);
  } else {
    // do nothing
  }

  console.log('UA-adjTSS: ', adjustment_type, hrTSS, duration, elev_gain, units, res);
  return res
}


function update_TSS() {
  let sport = get_workout_sport();

  if (sport) {
    let comment = get_adjTSS_comment();

    if (!comment) {
      let hrTSS = Number(getElementsByXPath(XP_hrTSS)[0].value);

      let duration_hms = getElementsByXPath('//input[@id = "totalTimeCompletedField"]')[0].value.split(':');
      let duration = Number(duration_hms[0]) * 3600 + Number(duration_hms[1]) * 60 + Number(duration_hms[2]);

      let elev_gain = Number(getElementsByXPath('//input[@id = "elevationGainCompletedField"]')[0].value);
      let units = getElementsByXPath('//div[@class = "workoutStatsColumn workoutStatsUnitLabel"]/label[@class = "elevation"]')[0].textContent === 'm' ? 'metric' : 'imperial';

      chrome.storage.sync.get(sport, function get_adjustment(sport_adjustment) {
        let adjTSS = calculate_adjTSS(sport_adjustment[sport], hrTSS, duration, elev_gain, units);

        if ((hrTSS > 0) && (adjTSS > 0)) {
          let elem_hrTSS = getElementsByXPath(XP_hrTSS)[0]
          elem_hrTSS.focus()
          elem_hrTSS.value = adjTSS.toString();
          elem_hrTSS.dispatchEvent(new Event('change', {
            bubbles: true
          }));

          let elem_comment = getElementsByXPath('//div[@id = "postActivityCommentsInput"]')[0]
          elem_comment.focus()
          elem_comment.innerText = ''.concat('UA-adjTSS: adjusted TSS from ', hrTSS.toString(), ' to ', adjTSS.toString());
          elem_comment.dispatchEvent(new Event('change', { bubbles: true }));
          elem_comment.dispatchEvent(new Event('input', { bubbles: true }));
          alert(''.concat('UA-adjTSS: adjusted TSS from ', hrTSS.toString(), ' to ', adjTSS.toString()));
        } else {
          alert('UA-adjTSS: cannot adjust - zero TSS');
        }
      });
    } else {
      alert('UA-adjTSS: workout already adjusted');
    }
  } else {
    alert('UA-adjTSS: workout summary is not open');
  }
}


function revert_TSS() {
  let sport = get_workout_sport();

  if (sport) {
    let comment = get_adjTSS_comment();

    if (comment) {
      let hrTSS = comment.split(' from ')[1].split(' to ')[0];

      let elem_hrTSS = getElementsByXPath(XP_hrTSS)[0]
      elem_hrTSS.focus()
      elem_hrTSS.value = hrTSS.toString();
      elem_hrTSS.dispatchEvent(new Event('change', { bubbles: true }));

      focus_click_elem(''.concat(XP_adjTSS_comment, '/following-sibling::div[@class = "deleteButton"]'));
      focus_click_elem('//div[@class = "dialogButtons"]/button[@id = "userConfirm" and @name = "ok"]');

      alert(''.concat('UA-adjTSS: adjusted TSS back to ', hrTSS.toString()));
    } else {
      alert('UA-adjTSS: workout was not adjusted');
    }
  } else {
    alert('UA-adjTSS: workout summary is not open');
  }
}


chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  console.log('UA-adjTSS: content received message', request);
  if (request.message === 'update_TSS') {
    update_TSS();
  } else if (request.message === 'revert_TSS') {
    revert_TSS();
  }
});


'use strict';


document.getElementById('menu-adjust-tss').addEventListener('click', function(e) {
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function(tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id, {
        message: 'update_TSS'
      });
  });
  window.close();
});


document.getElementById('menu-revert-tss').addEventListener('click', function(e) {
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function(tabs) {
    chrome.tabs.sendMessage(
      tabs[0].id, {
        message: 'revert_TSS'
      });
  });
  window.close();
});


// TODO maybe generate the table automatically
let sports = [
  'Run',
  'Bike',
  'Swim',
  'Brick',
  'Crosstrain',
  'MountainBike',
  'Strength',
  'Custom',
  'XC-Ski',
  'Rowing',
  'Other',
  'Walk'
];


function load_settings() {
  for (let i = 0; i < sports.length; i++) {
    chrome.storage.sync.get(sports[i], function get_adjustment(sport_adjustment) {
      let radios = document.getElementsByName(sports[i]);

      for (let j = 0; j < radios.length; j++) {
        if (radios[j].value == sport_adjustment[sports[i]]) {
          radios[j].checked = true;
          console.log('UA-adjTSS: ', sports[i], ' is set to ', radios[j].value);
        }

        radios[j].addEventListener('change', function(e) {
          let tmp = {};
          tmp[e.target.name] = e.target.value;
          chrome.storage.sync.set(tmp, function() {
            console.log('UA-adjTSS: ', tmp, ' saved');
          });
        });
      }
    });
  }
}

load_settings();

